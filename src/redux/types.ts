import { SortDirectionType } from 'react-virtualized'

export interface IGem {
    name: string
    sha: string
    version: string
    downloads: number
    project_uri: string
    info: string
    date_saved?: string
}

export const LOCAL_STORAGE_KEY = 'gems'

export type SORT_BY_KEYS = 'name' | 'project_uri' | 'info' | 'downloads' | 'date_saved'

export interface ISortBySetting {
    sortBy: SORT_BY_KEYS
    sortDirection: SortDirectionType
}

export type REDUCER_KEYS = 'search' | 'favorites' | 'sort_by'

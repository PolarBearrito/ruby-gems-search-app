import { selectSortedGems, selectSortedFavorites } from 'redux/selectors'

import { testUnsortedFavorites, testStore } from 'utils/fixtures'

describe('selectors /', () => {
    const storeState = {
        ...testStore,
        search: { ...testStore.search, gems: Object.values(testUnsortedFavorites) },
        favorites: { ...testStore.favorites, gems: testUnsortedFavorites },
    }
    storeState.sort_by.search = { sortBy: 'info', sortDirection: 'DESC' }
    storeState.sort_by.favorites = { sortBy: 'date_saved', sortDirection: 'ASC' }

    it('sorts search gems by info descending', () => {
        expect(selectSortedGems(storeState)).toMatchSnapshot()
    })

    it('sorts favorites gems by date_saved ascending', () => {
        expect(selectSortedFavorites(storeState)).toMatchSnapshot()
    })
})

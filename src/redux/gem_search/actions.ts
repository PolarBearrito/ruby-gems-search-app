import { createAction, TActionsUnion } from 'redux/create_action'
import { EActionTypes } from 'redux/gem_search/types'
import { IGem } from 'redux/types'

export const actions = {
    setQuery: (query: string) => createAction(EActionTypes.SET_QUERY, query),
    setLoading: () => createAction(EActionTypes.SET_LOADING),
    setLoadError: () => createAction(EActionTypes.SET_LOAD_ERROR),
    setAnyMore: (anyMore: boolean) => createAction(EActionTypes.SET_ANY_MORE, anyMore),
    setGems: (gems: IGem[]) => createAction(EActionTypes.SET_GEMS, gems),
    addGems: (gems: IGem[]) => createAction(EActionTypes.ADD_GEMS, gems),
    fetchNextPage: () => createAction(EActionTypes.FETCH_NEXT_PAGE),
}

export type TActions = TActionsUnion<typeof actions>

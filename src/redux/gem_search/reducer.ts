import { TActions } from 'redux/gem_search/actions'
import { EActionTypes } from 'redux/gem_search/types'
import { IGem } from 'redux/types'

export interface IStoreState {
    query: string
    loading: boolean
    error: boolean
    currentPage: number
    anyMore: boolean
    gems: IGem[]
}

export const initialState: IStoreState = {
    query: '',
    loading: false,
    error: false,
    currentPage: 1,
    anyMore: true,
    gems: [],
}

export default function(state = initialState, action: TActions): IStoreState {
    switch (action.type) {
        case EActionTypes.SET_QUERY:
            return { ...state, query: action.payload }
        case EActionTypes.SET_LOADING:
            return { ...state, loading: true, error: false }
        case EActionTypes.SET_LOAD_ERROR:
            return { ...state, loading: false, error: true }
        case EActionTypes.SET_ANY_MORE:
            return { ...state, anyMore: action.payload, loading: false, error: false }
        case EActionTypes.SET_GEMS:
            return {
                ...state,
                gems: action.payload,
                currentPage: 1,
                anyMore: true,
                loading: false,
                error: false,
            }
        case EActionTypes.ADD_GEMS:
            return {
                ...state,
                gems: [...state.gems, ...action.payload],
                currentPage: state.currentPage + 1,
                loading: false,
                error: false,
            }
    }
    return state
}

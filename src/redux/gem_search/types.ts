export enum EActionTypes {
    SET_QUERY = 'gem_search/set_query',
    SET_LOADING = 'gem_search/set_loading',
    SET_LOAD_ERROR = 'gem_search/set_load_error',
    SET_ANY_MORE = 'gem_search/set_any_more',
    SET_GEMS = 'gem_search/set_gems',
    ADD_GEMS = 'gem_search/add_gems',
    FETCH_NEXT_PAGE = 'gem_search/fetch_next_page',
}

import { actions } from 'redux/gem_search/actions'
import reducer, { IStoreState, initialState } from 'redux/gem_search/reducer'

import { createStore, Store } from 'redux'

import { testGem, testGem2 } from 'utils/fixtures'

describe('Search Sync Actions + Reducer /', () => {
    let reducerCheckingStore: Store<IStoreState>

    beforeEach(() => {
        reducerCheckingStore = createStore(reducer)
    })

    it('should start reducer with initialState', () => {
        expect(reducerCheckingStore.getState()).toEqual(initialState)
    })

    it('should setQuery with query abc then nothing', () => {
        reducerCheckingStore.dispatch(actions.setQuery('abc'))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.setQuery(''))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should setLoading, by setting loading to true and error to false', () => {
        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            loading: false,
            error: true,
        })
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.setLoading())
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should setLoadError, by setting loading to false and error to true', () => {
        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            loading: true,
            error: false,
        })
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.setLoadError())
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should setAnyMore with true and then false, resetting loading and error', () => {
        reducerCheckingStore.dispatch(actions.setAnyMore(true))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            query: '123',
            loading: true,
            error: true,
        })

        reducerCheckingStore.dispatch(actions.setAnyMore(false))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should setGems with testGem then testGem2, replacing gems and resetting currentPage, anyMore, loading, and error', () => {
        reducerCheckingStore.dispatch(actions.setGems([testGem]))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            query: '123',
            gems: [testGem],
            loading: true,
            error: true,
            currentPage: 1000,
            anyMore: false,
        })

        reducerCheckingStore.dispatch(actions.setGems([testGem2]))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should addGems with testGem then testGem2, adding gems, resetting loading and error and incrementing currentPage', () => {
        reducerCheckingStore.dispatch(actions.addGems([testGem]))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            query: '123',
            gems: [testGem],
            currentPage: 2,
            loading: true,
        })

        reducerCheckingStore.dispatch(actions.addGems([testGem2]))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should fetchNextPage and do nothing', () => {
        reducerCheckingStore.dispatch(actions.fetchNextPage())
        expect(reducerCheckingStore.getState()).toEqual(initialState)
    })
})

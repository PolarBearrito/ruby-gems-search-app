import { fetchGemsEpic, fetchNextPageEpic } from 'redux/gem_search/epic'
import { actions } from 'redux/gem_search/actions'

import { ICombinedStoreState } from 'redux/create_store'
import { ActionsObservable, StateObservable } from 'redux-observable'
import { Subject } from 'rxjs'

import { testGem, testGem2, testStore } from 'utils/fixtures'

import fetch from 'jest-fetch-mock'
import { toArray } from 'rxjs/operators'

describe('Search Epics /', () => {
    afterEach(() => {
        fetch.resetMocks()
    })

    describe('fetch first page of gems from correct API URL (123, page 1) /', () => {
        let action$: ActionsObservable<any>
        let state$: StateObservable<any>

        beforeEach(() => {
            action$ = ActionsObservable.of(actions.setQuery('123'))
            state$ = new StateObservable(new Subject<ICombinedStoreState>(), testStore)
        })

        it('has no query and dispatches setGems to []', async () => {
            fetch.mockResponseOnce(JSON.stringify([testGem, testGem2]))

            action$ = ActionsObservable.of(actions.setQuery(''))
            await fetchGemsEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(0)
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives data and dispatches setLoading and setGems', async () => {
            fetch.mockResponseOnce(JSON.stringify([testGem, testGem2]))

            await fetchGemsEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls[0]).toMatchSnapshot()
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives no data and dispatches setLoading and setAnyMore to false', async () => {
            fetch.mockResponseOnce(JSON.stringify([]))

            await fetchGemsEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls[0]).toMatchSnapshot()
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives rejection and dispatches setLoadError', async () => {
            fetch.mockReject(new Error('error'))

            await fetchGemsEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(1)
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives 403 and dispatches setLoadError', async () => {
            fetch.mockResponse(JSON.stringify([]), { status: 403 })

            await fetchGemsEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(1)
                    expect(outputActions).toMatchSnapshot()
                })
        })
    })

    describe('fetch next page of gems from correct API URL (123, page 2) /', () => {
        let action$: ActionsObservable<any>
        let state$: StateObservable<any>

        beforeEach(() => {
            action$ = ActionsObservable.of(actions.fetchNextPage())
            const modifiedTestStore = {
                ...testStore,
                search: { ...testStore.search, gems: [testGem], query: '123' },
            }
            state$ = new StateObservable(new Subject<ICombinedStoreState>(), modifiedTestStore)
        })

        it('receives new gems and dispatches setLoading and addGems', async () => {
            fetch.mockResponseOnce(JSON.stringify([testGem2]))

            await fetchNextPageEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls[0]).toMatchSnapshot()
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives no data and dispatches setLoading and setAnyMore to false', async () => {
            fetch.mockResponseOnce(JSON.stringify([]))

            await fetchNextPageEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(1)
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('dispatches no actions because there is nothing to fetch', async () => {
            fetch.mockResponseOnce(JSON.stringify([]))

            const modifiedTestStore = {
                ...testStore,
                search: { ...testStore.search, anyMore: false },
            }
            state$ = new StateObservable(new Subject<ICombinedStoreState>(), modifiedTestStore)

            await fetchNextPageEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(0)
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives 403 and dispatches setLoadError', async () => {
            fetch.mockResponse(JSON.stringify([]), { status: 403 })

            await fetchNextPageEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(1)
                    expect(outputActions).toMatchSnapshot()
                })
        })

        it('receives rejection and dispatches setLoadError', async () => {
            fetch.mockReject(new Error('error'))

            await fetchNextPageEpic(action$, state$)
                .pipe(toArray())
                .toPromise()
                .then(outputActions => {
                    expect(fetch.mock.calls.length).toEqual(1)
                    expect(outputActions).toMatchSnapshot()
                })
        })
    })
})

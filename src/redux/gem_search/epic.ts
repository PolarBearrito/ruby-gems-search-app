import { from as ObservableFrom, of as ObservableOf, concat, empty } from 'rxjs'
import { switchMap, map, debounceTime, catchError } from 'rxjs/operators'

import { actions } from 'redux/gem_search/actions'

import { ActionsObservable, StateObservable } from 'redux-observable'
import { EActionTypes } from 'redux/gem_search/types'
import { ICombinedStoreState } from 'redux/create_store'

export const fetchGemsEpic = (
    action$: ActionsObservable<ReturnType<typeof actions.setQuery>>,
    state$: StateObservable<ICombinedStoreState>
) =>
    action$.ofType(EActionTypes.SET_QUERY).pipe(
        debounceTime(500),
        switchMap(({ payload }) => {
            if (!payload) {
                return ObservableOf(actions.setGems([]))
            }

            return concat(
                ObservableOf(actions.setLoading()),
                ObservableFrom(fetchSearch(payload, 1)).pipe(
                    map(result => {
                        if (result.length === 0) {
                            return actions.setAnyMore(false)
                        }
                        return actions.setGems(result)
                    }),
                    catchError(error => ObservableOf(actions.setLoadError()))
                )
            )
        })
    )

export const fetchNextPageEpic = (
    action$: ActionsObservable<ReturnType<typeof actions.fetchNextPage>>,
    state$: StateObservable<ICombinedStoreState>
) =>
    action$.ofType(EActionTypes.FETCH_NEXT_PAGE).pipe(
        debounceTime(500),
        switchMap(() => {
            const { query, currentPage, anyMore } = state$.value.search
            if (!anyMore) {
                return empty()
            }

            return concat(
                ObservableOf(actions.setLoading()),
                ObservableFrom(fetchSearch(query, currentPage + 1)).pipe(
                    map(result => {
                        if (result.length === 0) {
                            return actions.setAnyMore(false)
                        }
                        return actions.addGems(result)
                    }),
                    catchError(error => ObservableOf(actions.setLoadError()))
                )
            )
        })
    )

const fetchSearch = (searchQuery: string, page: number) =>
    fetch(`//localhost:3000/api/v1/search.json?query=${searchQuery}&page=${page}`).then(response => {
        if (!response.ok) {
            throw Error(response.statusText)
        }
        return response.json()
    })

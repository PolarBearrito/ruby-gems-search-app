import { createStore, applyMiddleware, AnyAction, combineReducers } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import { combineEpics } from 'redux-observable'

import * as gemSearchEpics from 'redux/gem_search/epic'
import * as gemFavoriteEpics from 'redux/gem_favorite/epic'
import { default as gemSearchReducer, IStoreState as ISearchStoreState } from 'redux/gem_search/reducer'
import {
    default as gemFavoriteReducer,
    IStoreState as IFavoritesStoreState,
} from 'redux/gem_favorite/reducer'
import { default as gemSortByReducer, TSortableStoreState as ISortByStoreState } from 'redux/sort_by/reducer'

import { REDUCER_KEYS } from 'redux/types'

type TCombinedReducers = Record<REDUCER_KEYS, any>
export interface ICombinedStoreState {
    search: ISearchStoreState
    favorites: IFavoritesStoreState
    sort_by: ISortByStoreState
}
// for now this will check if ICombinedStoreState has missing keys
export type TVerifyICombinedStoreState<
    Missing extends never = Exclude<REDUCER_KEYS, keyof ICombinedStoreState>
> = ICombinedStoreState

const rootEpic = combineEpics(...Object.values(gemSearchEpics), ...Object.values(gemFavoriteEpics))
const epicMiddleware = createEpicMiddleware<AnyAction, AnyAction, ICombinedStoreState>()

const combinedReducers = {
    search: gemSearchReducer,
    favorites: gemFavoriteReducer,
    sort_by: gemSortByReducer,
}
const reducers = combineReducers<TCombinedReducers>(combinedReducers)

const store = createStore(reducers, applyMiddleware(epicMiddleware))

epicMiddleware.run(rootEpic)

export default store

// TODO: create helpers that take a type like TCombinedReducers to enforce that the passed in objects
//      use the reducer keys

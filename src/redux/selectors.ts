import { createSelector } from 'reselect'
import { orderBy } from 'lodash'

import { ICombinedStoreState } from 'redux/create_store'
import { IGem, ISortBySetting } from 'redux/types'
import { IGemDict } from 'redux/gem_favorite/reducer'

type TLowerCaseDirection = 'asc' | 'desc'

export const selectSortedGems = createSelector<ICombinedStoreState, IGem[], ISortBySetting, IGem[]>(
    state => state.search.gems,
    state => state.sort_by.search,
    (gems, { sortBy, sortDirection }) =>
        orderBy(gems, sortBy, sortDirection.toLowerCase() as TLowerCaseDirection)
)

export const selectSortedFavorites = createSelector<ICombinedStoreState, IGemDict, ISortBySetting, IGem[]>(
    state => state.favorites.gems,
    state => state.sort_by.favorites,
    (gems, { sortBy, sortDirection }) =>
        orderBy(Object.values(gems), sortBy, sortDirection.toLowerCase() as TLowerCaseDirection)
)

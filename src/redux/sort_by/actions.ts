import { createAction, TActionsUnion } from 'redux/create_action'
import { EActionTypes } from 'redux/sort_by/types'
import { ISortBySetting, REDUCER_KEYS } from 'redux/types'

export const actions = {
    setSortBy: (reducer: REDUCER_KEYS, sortBySetting: ISortBySetting) =>
        createAction(EActionTypes.SET_SORT_BY, sortBySetting, reducer),
}

export type TActions = TActionsUnion<typeof actions>

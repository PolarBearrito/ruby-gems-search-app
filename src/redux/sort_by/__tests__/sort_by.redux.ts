import { actions } from 'redux/sort_by/actions'
import reducer, { TSortableStoreState, initialState } from 'redux/sort_by/reducer'

import { createStore, Store } from 'redux'

import { ISortBySetting } from 'redux/types'

describe('Sort By Sync Actions + Reducer /', () => {
    let reducerCheckingStore: Store<TSortableStoreState>

    beforeEach(() => {
        reducerCheckingStore = createStore(reducer)
    })

    it('should start reducer with initialState', () => {
        expect(reducerCheckingStore.getState()).toEqual(initialState)
    })

    it('should setSortBy with sort by setting (name, DESC) and update the search reducer', () => {
        const fakeSortBy = { sortBy: 'name', sortDirection: 'DESC' } as ISortBySetting
        reducerCheckingStore.dispatch(actions.setSortBy('search', fakeSortBy))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should setSortBy with sort by setting (info, ASC) and update the favorites reducer', () => {
        const fakeSortBy = { sortBy: 'info', sortDirection: 'ASC' } as ISortBySetting
        reducerCheckingStore.dispatch(actions.setSortBy('favorites', fakeSortBy))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })
})

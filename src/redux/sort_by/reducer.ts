import { TActions } from 'redux/sort_by/actions'
import { EActionTypes } from 'redux/sort_by/types'
import { ISortBySetting, REDUCER_KEYS } from 'redux/types'

type SORTABLE_REDUCER_KEYS = Extract<REDUCER_KEYS, 'search' | 'favorites'>
export type TSortableStoreState = { [reducer in SORTABLE_REDUCER_KEYS]: ISortBySetting }

export const initialState: TSortableStoreState = {
    search: { sortBy: 'downloads', sortDirection: 'DESC' },
    favorites: { sortBy: 'date_saved', sortDirection: 'DESC' },
}

export default function(state = initialState, action: TActions): TSortableStoreState {
    switch (action.type) {
        case EActionTypes.SET_SORT_BY:
            switch (action.reducerKey) {
                case 'search':
                    return { ...state, search: { ...action.payload } }
                case 'favorites':
                    return { ...state, favorites: { ...action.payload } }
            }
    }
    return state
}

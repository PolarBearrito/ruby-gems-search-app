// adapted: https://medium.com/@martin_hotell/improved-redux-type-safety-with-typescript-2-8-2c11a8062575
// TODO: create a better way to generate a source of truth, everything should extend from a enum of action types
// and a payload dict that maps each action type to a payload possibility
// reducer and generated actions must have a handler and a create action fn for every action type

export interface IAction<T extends string> {
    type: T
}

export interface IActionWithPayload<T extends string, P> extends IAction<T> {
    payload: P
}
export interface IActionWithPayload<T extends string, P> extends IAction<T> {
    payload: P
}
export interface IActionWithPayloadReducer<T extends string, P, R extends string>
    extends IActionWithPayload<T, P> {
    reducerKey: R
}

export function createAction<T extends string>(type: T): IAction<T>
export function createAction<T extends string, P>(type: T, payload: P): IActionWithPayload<T, P>
export function createAction<T extends string, P, R extends string>(
    type: T,
    payload: P,
    reducerKey: R
): IActionWithPayloadReducer<T, P, R>
export function createAction<T extends string, P, R extends string>(type: T, payload?: P, reducerKey?: R) {
    return {
        type,
        ...(payload === undefined ? {} : { payload }),
        ...(reducerKey === undefined ? {} : { reducerKey }),
    }
}

export type TActionsUnion<A extends { [actionCreator: string]: (...args: any[]) => any }> = ReturnType<
    A[keyof A]
>

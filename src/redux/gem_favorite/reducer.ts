import { EActionTypes } from 'redux/gem_favorite/types'
import { TActions } from 'redux/gem_favorite/actions'
import { IGem, LOCAL_STORAGE_KEY } from 'redux/types'

export interface IGemDict {
    [shaHash: string]: IGem
}

export interface IStoreState {
    gems: IGemDict
}

export const initialState: IStoreState = {
    gems: JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY) || '{}'),
}

export default function(state = initialState, action: TActions): IStoreState {
    switch (action.type) {
        case EActionTypes.ADD_FAVORITE:
            return { ...state, gems: { ...state.gems, [action.payload.sha]: action.payload } }
        case EActionTypes.REMOVE_FAVORITE:
            const key = action.payload.sha
            // can't fully cover, tslib rest has a ternary that checks if key is a Symbol,
            // even though typescript itself blocks that as key is declared as a string
            const { [key]: removedGem, ...rest } = state.gems
            return { ...state, gems: { ...rest } }
    }
    return state
}

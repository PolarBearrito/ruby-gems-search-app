import { actions } from 'redux/gem_favorite/actions'
import reducer, { IStoreState, initialState } from 'redux/gem_favorite/reducer'

import { createStore, Store } from 'redux'

import { testGem, testGem2, testEmptyGem } from 'utils/fixtures'

describe('Favorites Sync Actions + Reducer /', () => {
    let reducerCheckingStore: Store<IStoreState>

    beforeEach(() => {
        reducerCheckingStore = createStore(reducer)
    })

    it('should start reducer with initialState', () => {
        expect(reducerCheckingStore.getState()).toEqual(initialState)
    })

    it('should addFavorite with testGem then testGem2', () => {
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.addFavorite(testGem))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.addFavorite(testGem2))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })

    it('should removeFavorite with no gems removed, then testGem removed', () => {
        reducerCheckingStore = createStore(reducer, {
            ...initialState,
            gems: {
                [testGem.sha]: testGem,
                [testGem2.sha]: testGem2,
            },
        })
        expect(reducerCheckingStore.getState()).toMatchSnapshot()

        reducerCheckingStore.dispatch(actions.removeFavorite(testEmptyGem))
        expect(Object.values(reducerCheckingStore.getState().gems).length).toEqual(2)

        reducerCheckingStore.dispatch(actions.removeFavorite(testGem))
        expect(reducerCheckingStore.getState()).toMatchSnapshot()
    })
})

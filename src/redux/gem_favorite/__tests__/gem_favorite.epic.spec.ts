import { addFavoriteEpic, removeFavoriteEpic } from 'redux/gem_favorite/epic'
import { actions } from 'redux/gem_favorite/actions'

import { ICombinedStoreState } from 'redux/create_store'
import { ActionsObservable, StateObservable } from 'redux-observable'
import { Subject } from 'rxjs'

import { testGem, testGem2, testStore } from 'utils/fixtures'

describe('Favorites Epics /', () => {
    it('adds testGem to localStorage when there is an add favorite action with testGem', async () => {
        const action = ActionsObservable.of(actions.addFavorite(testGem))
        const state$ = new StateObservable(new Subject<ICombinedStoreState>(), testStore)

        expect(localStorage.store.gems).toBeUndefined()

        const outputAction = await addFavoriteEpic(action, state$).toPromise()
        expect(outputAction).toBeUndefined()
        expect(JSON.parse(localStorage.store.gems)).toMatchSnapshot()
    })

    it('removes testGem from localStorage when there is a remove favorite action with testGem', async () => {
        const action = ActionsObservable.of(actions.removeFavorite(testGem))
        const state$ = new StateObservable(new Subject<ICombinedStoreState>(), testStore)

        localStorage.setItem('gems', JSON.stringify({ [testGem2.sha]: testGem2, [testGem.sha]: testGem }))
        expect(JSON.parse(localStorage.store.gems)).toMatchSnapshot()

        const outputAction = await removeFavoriteEpic(action, state$).toPromise()
        expect(outputAction).toBeUndefined()
        expect(JSON.parse(localStorage.store.gems)).toMatchSnapshot()
    })
})

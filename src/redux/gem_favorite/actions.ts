import { createAction, TActionsUnion } from 'redux/create_action'
import { EActionTypes } from 'redux/gem_favorite/types'
import { IGem } from 'redux/types'

export const actions = {
    addFavorite: (gem: IGem) =>
        createAction(EActionTypes.ADD_FAVORITE, { ...gem, date_saved: new Date().toISOString() }),
    removeFavorite: (gem: IGem) =>
        createAction(EActionTypes.REMOVE_FAVORITE, { ...gem, date_saved: new Date().toISOString() }),
}

export type TActions = TActionsUnion<typeof actions>

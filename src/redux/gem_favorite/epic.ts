import { empty } from 'rxjs'
import { mergeMap } from 'rxjs/operators'

import { actions } from 'redux/gem_favorite/actions'

import { ActionsObservable, StateObservable } from 'redux-observable'
import { EActionTypes } from 'redux/gem_favorite/types'
import { ICombinedStoreState } from 'redux/create_store'
import { LOCAL_STORAGE_KEY } from 'redux/types'

export const addFavoriteEpic = (
    action$: ActionsObservable<ReturnType<typeof actions.addFavorite>>,
    state$: StateObservable<ICombinedStoreState>
) =>
    action$.ofType(EActionTypes.ADD_FAVORITE).pipe(
        mergeMap(({ payload }) => {
            const newGems = {
                ...JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY) || '{}'),
                [payload.sha]: payload,
            }
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newGems))
            return empty()
        })
    )

export const removeFavoriteEpic = (
    action$: ActionsObservable<ReturnType<typeof actions.removeFavorite>>,
    state$: StateObservable<ICombinedStoreState>
) =>
    action$.ofType(EActionTypes.REMOVE_FAVORITE).pipe(
        mergeMap(({ payload }) => {
            const removeKey = payload.sha
            const { [removeKey]: removedGem, ...rest } = JSON.parse(
                localStorage.getItem(LOCAL_STORAGE_KEY) || '{}'
            )
            const newGems = { ...rest }
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newGems))
            return empty()
        })
    )

export enum EActionTypes {
    ADD_FAVORITE = 'gem_favorite/add_favorite',
    REMOVE_FAVORITE = 'gem_favorite/remove_favorite',
}

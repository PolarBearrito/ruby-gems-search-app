import React from 'react'
import Icon from '@mdi/react'

// helpers
import { mdiMagnify, mdiDiamondStone, mdiDiamond, mdiLoading } from '@mdi/js'

// redux
import { connect } from 'react-redux'
import { actions, TActions } from 'redux/gem_search/actions'

// types
import { Dispatch } from 'redux'
import { ICombinedStoreState } from 'redux/create_store'
import { IGem } from 'redux/types'
import { History } from 'history'

import styles from 'components/search_form.module.scss'
import classnames from 'classnames'

export interface IProps {
    query: string
    gems: IGem[]
    anyMore: boolean
    loading: boolean
    error: boolean
    setQuery: (query: string) => void
    history: History
}

export interface IState {
    firstSearchDone: boolean // tracks if a search has been performed since this component was mounted
}

export class SearchForm extends React.Component<IProps, IState> {
    static getDerivedStateFromProps(nextProps: IProps) {
        let state = null

        if (nextProps.gems.length) {
            state = { ...(state || {}), firstSearchDone: true }
        }

        return state
    }

    constructor(props: IProps) {
        super(props)

        this.state = {
            firstSearchDone: false,
        }
    }

    handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.setQuery(event.target.value)
    }

    goToFavorites = (event: React.MouseEvent<HTMLButtonElement>) => {
        this.props.history.push('/saved')
    }

    render() {
        return (
            <div
                className={classnames(
                    styles.header,
                    styles.left,
                    !this.state.firstSearchDone && styles.noSearch
                )}
            >
                <div className={styles.gem}>
                    <Icon
                        path={mdiDiamondStone}
                        size={this.state.firstSearchDone ? 2 : 5}
                        color="rgba(255, 100, 100, 1)"
                    />
                </div>
                {!this.state.firstSearchDone && <div className={styles.title}>Ruby Gems Search</div>}
                <div className={classnames(styles.searchForm)}>
                    <div className={styles.searchBox}>
                        <input
                            type="text"
                            name="ruby"
                            className={styles.searchField}
                            value={this.props.query}
                            onChange={this.handleChange}
                        />
                        <span className={styles.searchIcon}>
                            <Icon
                                path={this.props.loading ? mdiLoading : mdiMagnify}
                                size={1}
                                spin={this.props.loading}
                                color="rgba(0, 0, 0, 0.5)"
                            />
                        </span>
                        {this.props.error && <div className={styles.errorMessage}>error searching.</div>}
                    </div>
                    <div className={styles.favoritesButtonWrap}>
                        <button className={styles.favoritesButton} onClick={this.goToFavorites}>
                            <Icon path={mdiDiamond} size={1} color="rgba(255, 100, 100, 1)" />
                            View Saved
                        </button>
                    </div>
                </div>
                {this.state.firstSearchDone && (
                    <div className={styles.progress}>
                        {Boolean(this.props.gems.length) &&
                            (this.props.loading
                                ? 'loading...'
                                : `loaded ${this.props.gems.length} of ${
                                      this.props.anyMore ? '?' : this.props.gems.length
                                  }`)}
                    </div>
                )}
            </div>
        )
    }
}

export const mapStateToProps = (state: ICombinedStoreState) => state.search
export const mapDispatchToProps = (dispatch: Dispatch<TActions>) => {
    return {
        setQuery: (query: string) => dispatch(actions.setQuery(query)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchForm)

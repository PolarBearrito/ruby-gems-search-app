import React from 'react'
import List from 'components/core/list'

// redux
import { connect } from 'react-redux'
import { actions as searchActions } from 'redux/gem_search/actions'
import { actions as favoriteActions } from 'redux/gem_favorite/actions'
import { actions as sortByActions } from 'redux/sort_by/actions'
import { selectSortedGems } from 'redux/selectors'

// types
import { Dispatch, AnyAction } from 'redux'
import { SortDirectionType } from 'react-virtualized'
import { IGemDict } from 'redux/gem_favorite/reducer'
import { IGem, SORT_BY_KEYS, ISortBySetting } from 'redux/types'
import { ICombinedStoreState } from 'redux/create_store'

interface IReduxState {
    loading: boolean
    gems: any[]
    anyMore: boolean
    favorites: IGemDict
    sortBy: SORT_BY_KEYS
    sortDirection: SortDirectionType
}

interface IReduxDispatch {
    fetchNext: () => Promise<any>
    addFavorite: (gem: IGem) => void
    removeFavorite: (gem: IGem) => void
    sort: (sortBySetting: ISortBySetting) => void
}

export interface IProps extends IReduxState, IReduxDispatch {}

export class SearchResults extends React.Component<IProps> {
    rowCount = () =>
        this.props.gems.length === 0
            ? 0
            : this.props.anyMore
            ? this.props.gems.length + 15
            : this.props.gems.length

    loadMoreRows = () => (this.props.loading ? () => Promise.resolve(null) : this.props.fetchNext)

    isRowLoaded = ({ index }: { index: number }) => !this.props.anyMore || index < this.props.gems.length

    render() {
        const { gems, favorites, addFavorite, removeFavorite, sort, sortBy, sortDirection } = this.props
        return (
            <div>
                <List
                    isRowLoaded={this.isRowLoaded}
                    loadMoreRows={this.loadMoreRows()}
                    rowCount={this.rowCount()}
                    gems={gems}
                    favorites={favorites}
                    addFavorite={addFavorite}
                    removeFavorite={removeFavorite}
                    sortBy={sortBy}
                    sortDirection={sortDirection}
                    sort={sort}
                />
            </div>
        )
    }
}

export const mapStateToProps = (state: ICombinedStoreState) => ({
    ...state.search,
    gems: selectSortedGems(state),
    favorites: state.favorites.gems,
    ...state.sort_by.search,
})
export const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        fetchNext: () => {
            dispatch(searchActions.fetchNextPage())
            return Promise.resolve([])
        },
        addFavorite: (gem: IGem) => dispatch(favoriteActions.addFavorite(gem)),
        removeFavorite: (gem: IGem) => dispatch(favoriteActions.removeFavorite(gem)),
        sort: (sortBySetting: ISortBySetting) => dispatch(sortByActions.setSortBy('search', sortBySetting)),
    }
}

export default connect<IReduxState, IReduxDispatch>(
    mapStateToProps,
    mapDispatchToProps
)(SearchResults)

import * as React from 'react'
import { shallow } from 'enzyme'

import { SearchResults, IProps, mapStateToProps, mapDispatchToProps } from 'components/search_results'
import List from 'components/core/list'

import { testFavorites, testStore, testEmptyGem } from 'utils/fixtures'

describe('Search Results /', () => {
    let props = {} as IProps

    beforeEach(() => {
        props = {
            loading: false,
            anyMore: false,
            gems: [],
            favorites: testFavorites,
            sortBy: 'downloads',
            sortDirection: 'ASC',
            sort: () => ({}),
            addFavorite: () => ({}),
            removeFavorite: () => ({}),
            fetchNext: jest.fn(() => Promise.resolve([])),
        }
    })

    describe('rendering /', () => {
        let wrapper: ReturnType<typeof shallow>

        afterEach(() => {
            expect(wrapper).toMatchSnapshot()
        })

        it('renders basic', () => {
            wrapper = shallow(<SearchResults {...props} />)
        })

        it('renders with 1 gem with rowCount of 1', () => {
            props.gems = [testEmptyGem]
            wrapper = shallow(<SearchResults {...props} />)
        })

        it('renders with 1 gem and anyMore as true with rowCount of 16', () => {
            props.gems = [testEmptyGem]
            props.anyMore = true
            wrapper = shallow(<SearchResults {...props} />)
        })
    })

    describe('internal handlers /', () => {
        it('calls fetchNext prop with loadMoreRows', () => {
            const wrapper = shallow(<SearchResults {...props} />)
            const fakeIndexRange = { startIndex: 1, stopIndex: 1 }

            expect(props.fetchNext).toHaveBeenCalledTimes(0)
            wrapper.find(List).prop('loadMoreRows')!(fakeIndexRange)

            expect(props.fetchNext).toHaveBeenCalledTimes(1)
            expect(props.fetchNext).toHaveBeenCalledWith(fakeIndexRange)
        })

        it('calls nothing with loadMoreRows when prop loading is true', async () => {
            props.loading = true
            const wrapper = shallow(<SearchResults {...props} />)

            expect(props.fetchNext).toHaveBeenCalledTimes(0)
            const result = await wrapper.find(List).prop('loadMoreRows')!({ startIndex: 1, stopIndex: 1 })

            expect(props.fetchNext).toHaveBeenCalledTimes(0)
            expect(result).toEqual(null)
        })

        it('should output false for isRowLoaded for index 2 with 1 gem and anyMore = true', () => {
            props.gems = [testEmptyGem]
            props.anyMore = true
            const wrapper = shallow(<SearchResults {...props} />)
            const fakeIndex = { index: 2 }

            const result = wrapper.find(List).prop('isRowLoaded')!(fakeIndex)
            expect(result).toEqual(false)
        })

        // should be impossible conditions, but just in case:
        it('should output true for isRowLoaded for index 2 with 1 gem and anyMore = false', () => {
            props.gems = [testEmptyGem]
            props.anyMore = false
            const wrapper = shallow(<SearchResults {...props} />)
            const fakeIndex = { index: 2 }

            const result = wrapper.find(List).prop('isRowLoaded')!(fakeIndex)
            expect(result).toEqual(true)
        })

        it('should output true for isRowLoaded for index 1 with 1 gem and anyMore = true', () => {
            props.gems = [testEmptyGem]
            props.anyMore = true
            const wrapper = shallow(<SearchResults {...props} />)
            const fakeIndex = { index: 2 }

            const result = wrapper.find(List).prop('isRowLoaded')!(fakeIndex)
            expect(result).toEqual(false)
        })
    })

    describe('redux connect /', () => {
        it('mapsStateToProps', () => {
            expect(mapStateToProps(testStore)).toMatchSnapshot()
        })

        describe('mapsDispatchToProps /', () => {
            let fakeDispatch: ReturnType<typeof jest.fn>
            let mapped: ReturnType<typeof mapDispatchToProps>

            beforeEach(() => {
                fakeDispatch = jest.fn(action => action)
                mapped = mapDispatchToProps(fakeDispatch)
            })

            afterEach(() => {
                expect(fakeDispatch.mock.calls).toMatchSnapshot()
            })

            it('produces fetch next page action and returns [] with fetchNext', async () => {
                const result = await mapped.fetchNext()
                expect(result).toEqual([])
            })

            it('produces add favorite action with empty test gem when addFavorite and empty test gem', () => {
                mapped.addFavorite(testEmptyGem)
            })

            it('produces remove favorite action with empty test gem when removeFavorite and empty test gem', () => {
                mapped.removeFavorite(testEmptyGem)
            })

            it('produces set sort by action to correct reducer with sortBy info, sortDirection ASC', () => {
                mapped.sort({ sortBy: 'info', sortDirection: 'ASC' })
            })
        })
    })
})

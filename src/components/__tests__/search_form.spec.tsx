import * as React from 'react'
import { shallow } from 'enzyme'
import { createBrowserHistory } from 'history'

import { SearchForm, IProps, IState, mapStateToProps, mapDispatchToProps } from 'components/search_form'

import { testGems, testStore, testEmptyGem } from 'utils/fixtures'

describe('Search Form /', () => {
    let props = {} as IProps

    beforeEach(() => {
        props = {
            query: '',
            loading: false,
            error: false,
            anyMore: false,
            gems: [],
            setQuery: jest.fn(),
            history: createBrowserHistory(),
        }
    })

    describe('rendering /', () => {
        let wrapper: ReturnType<typeof shallow>
        let state: IState

        beforeEach(() => {
            state = { firstSearchDone: false }
        })

        afterEach(() => {
            wrapper = shallow(<SearchForm {...props} />)
            wrapper.setState(state)
            expect(wrapper).toMatchSnapshot()
        })

        it('renders basic', () => undefined)

        it('renders with fetch error, showing error message', () => {
            props.error = true
        })

        it('renders with firstSearchDone as true', () => {
            state.firstSearchDone = true
        })

        it('renders with firstSearchDone as true and fetch error, showing error message', () => {
            props.error = true
            state.firstSearchDone = true
        })

        it('renders with loading, with spinner icon', () => {
            props.loading = true
        })

        it('renders with gems and anyMore as false, with progress showing gem count', () => {
            props.gems = [testEmptyGem]
            props.anyMore = false
        })

        it('renders with gems and loading, with progress showing loading...', () => {
            props.gems = [testEmptyGem]
            props.loading = true
        })

        it('renders with gems and anyMore as true, with progress showing loading more out of ?', () => {
            props.gems = [testEmptyGem]
            props.anyMore = true
        })
    })

    describe('prop change /', () => {
        it('sets state firstSearchDone to true when gems is passed in', () => {
            const wrapper = shallow(<SearchForm {...props} />)
            expect(wrapper.state()).toMatchSnapshot()
            wrapper.setProps({
                gems: testGems,
            })
            expect(wrapper.state()).toMatchSnapshot()
        })
    })

    describe('internal handlers /', () => {
        it('calls setQuery prop upon onChange event', () => {
            const wrapper = shallow(<SearchForm {...props} />)
            const fakeQuery = 'blah'

            expect(props.setQuery).toHaveBeenCalledTimes(0)
            wrapper.find('input').simulate('change', { target: { value: fakeQuery } })

            expect(props.setQuery).toHaveBeenCalledTimes(1)
            expect(props.setQuery).toHaveBeenCalledWith(fakeQuery)
        })

        it('calls history push upon button click event to go to favorites', () => {
            props.history.push = jest.fn()
            const wrapper = shallow(<SearchForm {...props} />)

            expect(props.history.push).toHaveBeenCalledTimes(0)
            wrapper.find('button').simulate('click')

            expect(props.history.push).toHaveBeenCalledTimes(1)
            expect(props.history.push).toHaveBeenCalledWith('/saved')
        })
    })

    describe('redux connect /', () => {
        it('mapsStateToProps', () => {
            expect(mapStateToProps(testStore)).toMatchSnapshot()
        })

        it('mapsDispatchToProps with setQuery which produces the setQuery action with abc as the query', () => {
            const fakeDispatch = jest.fn(action => action)
            const mapped = mapDispatchToProps(fakeDispatch)

            mapped.setQuery('abc')
            expect(fakeDispatch.mock.calls).toMatchSnapshot()
        })
    })
})

import React from 'react'

// helpers
import { mdiDiamondOutline, mdiDiamond } from '@mdi/js'
import generateDummyText from 'utils/generate_dummy_text'

// types
import { IGem } from 'redux/types'

import styles from 'components/core/row.module.scss'
import classnames from 'classnames'

export interface IProps {
    index: number
    rowData?: IGem
    favorite: boolean
    addFavorite: (gem: IGem) => void
    removeFavorite: (gem: IGem) => void
}

export default class ListRow extends React.Component<IProps> {
    addFave = (e: React.MouseEvent<HTMLButtonElement>) => {
        const { rowData, addFavorite } = this.props
        addFavorite(rowData!)
        e.preventDefault()
    }

    removeFave = (e: React.MouseEvent<HTMLButtonElement>) => {
        const { rowData, removeFavorite } = this.props
        removeFavorite(rowData!)
        e.preventDefault()
    }

    render() {
        const { index, rowData, favorite } = this.props

        if (!rowData) {
            const nameWidth = generateDummyText(index, 200)
            const linkWidth = generateDummyText(index + 1, 150, 40)
            const infoWidth = generateDummyText(index + 2, 250, 25)
            const dlWidth = generateDummyText(index + 3, 75, 5)

            return (
                <div className={classnames(styles.row, styles.dummy)}>
                    <div className={styles.textData}>
                        <div className={styles.title}>
                            <div className={styles.name} style={{ width: nameWidth }} />
                        </div>
                        <div className={styles.link} style={{ width: linkWidth }} />
                        <div className={styles.description} style={{ width: infoWidth }} />
                    </div>
                    <div className={styles.downloads}>
                        <div className={styles.dlCount} style={{ width: dlWidth }} />
                        <div className={styles.dlText}>downloads</div>
                    </div>
                    <div className={styles.favoriteButton}>
                        <button className={styles.addFavoriteButton}>
                            <svg
                                viewBox="0 0 24 24"
                                className={classnames(styles.addFavoriteIcon, styles.outline)}
                            >
                                <path d={mdiDiamondOutline} />
                            </svg>
                        </button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className={styles.row}>
                    <a className={styles.rowLink} href={rowData.project_uri} />
                    <div className={styles.textData}>
                        <div className={styles.title}>
                            <div className={styles.name}>{rowData.name}</div>
                            <div className={styles.version}>{`v${rowData.version}`}</div>
                            {rowData.date_saved && (
                                <div className={styles.dateSaved}>{`(saved: ${new Date(
                                    rowData.date_saved
                                ).toDateString()}, ${new Date(
                                    rowData.date_saved
                                ).toLocaleTimeString()})`}</div>
                            )}
                        </div>
                        <a className={styles.link} href={rowData.project_uri}>
                            {rowData.project_uri}
                        </a>
                        <div className={styles.description}>{rowData.info}</div>
                    </div>
                    <div className={styles.downloads}>
                        <div className={styles.dlCount}>{rowData.downloads}</div>
                        <div className={styles.dlText}>downloads</div>
                    </div>
                    <div className={styles.favoriteButton}>
                        <button
                            onClick={favorite ? this.removeFave : this.addFave}
                            className={styles.addFavoriteButton}
                        >
                            <svg
                                viewBox="0 0 24 24"
                                className={classnames(styles.addFavoriteIcon, styles.outline)}
                            >
                                <path d={mdiDiamondOutline} />
                            </svg>
                            {favorite && (
                                <svg viewBox="0 0 24 24" className={styles.addFavoriteIcon}>
                                    <path d={mdiDiamond} />
                                </svg>
                            )}
                        </button>
                    </div>
                </div>
            )
        }
    }
}

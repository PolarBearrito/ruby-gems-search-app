import React from 'react'
import ListRow from 'components/core/row'
import {
    InfiniteLoader,
    AutoSizer,
    Table as VirtualizedList,
    Column,
    SortIndicator,
    Index,
    IndexRange,
    TableRowProps,
    TableHeaderProps,
    SortDirectionType,
} from 'react-virtualized'

// types
import { IGem, ISortBySetting, SORT_BY_KEYS } from 'redux/types'
import { IGemDict } from 'redux/gem_favorite/reducer'

import styles from 'components/core/list.module.scss'
import classnames from 'classnames'

type ISortBy = { [key in SORT_BY_KEYS]: string }

export interface IProps {
    isRowLoaded?: (params: Index) => boolean
    loadMoreRows?: (params: IndexRange) => Promise<any>
    rowCount: number

    isFavorites?: boolean
    gems: IGem[]
    favorites: IGemDict
    addFavorite: (gem: IGem) => void
    removeFavorite: (gem: IGem) => void

    sortBy: SORT_BY_KEYS
    sortDirection: SortDirectionType
    sort: (sortBy: ISortBySetting) => void
}

export default class List extends React.Component<IProps> {
    static defaultProps = {
        isRowLoaded: () => true,
        loadMoreRows: () => Promise.resolve(null),
    }

    dataKeyToColumnName: ISortBy = {
        name: 'Name',
        project_uri: 'URL',
        info: 'Description',
        downloads: 'Downloads',
        date_saved: 'Date Saved',
    }

    getRowClassName = ({ index }: Index) => (index < 0 ? styles.tableHeader : '')

    headerRenderer = ({ dataKey, sortBy, sortDirection }: TableHeaderProps) => {
        const sortedBy = sortBy === dataKey
        return (
            <div className={classnames(styles.columnHeader, sortedBy && styles.bold)}>
                {this.dataKeyToColumnName[dataKey]}
                {sortedBy && <SortIndicator sortDirection={sortDirection} />}
            </div>
        )
    }

    rowGetter = ({ index }: { index: number }) => this.props.gems[index] || false // false for loading rows or else a destructure 'undefined' error

    rowRenderer = ({ index, rowData, style }: TableRowProps) => {
        const { favorites, addFavorite, removeFavorite } = this.props

        return (
            <div key={index} style={style}>
                <ListRow
                    index={index}
                    rowData={rowData}
                    favorite={Boolean(rowData && favorites[rowData.sha])}
                    addFavorite={addFavorite}
                    removeFavorite={removeFavorite}
                />
            </div>
        )
    }

    render() {
        const { isRowLoaded, loadMoreRows, rowCount, ...otherProps } = this.props
        if (!otherProps.gems.length) {
            return (
                <div className={styles.noResults}>
                    {otherProps.isFavorites ? 'No saved gems.' : 'No results.'}
                </div>
            )
        }
        return (
            <div>
                <InfiniteLoader isRowLoaded={isRowLoaded!} loadMoreRows={loadMoreRows!} rowCount={rowCount}>
                    {({ onRowsRendered, registerChild }) => (
                        <AutoSizer disableHeight>
                            {({ width }) => (
                                <VirtualizedList
                                    ref={registerChild}
                                    onRowsRendered={onRowsRendered}
                                    rowClassName={this.getRowClassName}
                                    rowRenderer={this.rowRenderer}
                                    rowGetter={this.rowGetter}
                                    rowHeight={93}
                                    height={600}
                                    rowCount={rowCount}
                                    width={width}
                                    headerHeight={32}
                                    headerClassName={styles.columnHeaderWrap}
                                    {...otherProps} // trigger re-render when favorites or gems change
                                >
                                    <Column
                                        dataKey="name"
                                        width={64}
                                        headerRenderer={this.headerRenderer}
                                        flexGrow={1}
                                    />
                                    <Column
                                        dataKey="project_uri"
                                        width={52}
                                        headerRenderer={this.headerRenderer}
                                        flexGrow={1}
                                    />
                                    {otherProps.isFavorites && (
                                        <Column
                                            dataKey="date_saved"
                                            width={110}
                                            headerRenderer={this.headerRenderer}
                                            flexGrow={1}
                                        />
                                    )}
                                    <Column
                                        dataKey="info"
                                        width={90}
                                        headerRenderer={this.headerRenderer}
                                        flexGrow={10}
                                    />
                                    <Column
                                        dataKey="downloads"
                                        width={114}
                                        headerRenderer={this.headerRenderer}
                                        flexGrow={1}
                                    />
                                </VirtualizedList>
                            )}
                        </AutoSizer>
                    )}
                </InfiniteLoader>
            </div>
        )
    }
}

import * as React from 'react'
import { shallow } from 'enzyme'

import Row, { IProps } from 'components/core/row'

import { testEmptyGem, testFavorites } from 'utils/fixtures'

describe('Row /', () => {
    let props = {} as IProps

    beforeEach(() => {
        props = {
            index: 0,
            rowData: undefined,
            favorite: true,
            addFavorite: jest.fn(() => ({})),
            removeFavorite: jest.fn(() => ({})),
        }
    })

    describe('rendering /', () => {
        afterEach(() => {
            expect(shallow(<Row {...props} />)).toMatchSnapshot()
        })

        it('renders no gem with dummy row', () => {
            props.rowData = undefined
        })

        it('renders with gem as favorited with 2 favorite icons', () => {
            props.rowData = testEmptyGem
            props.favorite = true
        })

        it('renders with gem as not favorited with 1 favorite icon', () => {
            props.rowData = testEmptyGem
            props.favorite = false
        })

        it('renders with favorites gem with date_saved data', () => {
            props.rowData = Object.values(testFavorites)[0]
        })
    })

    describe('internal handlers /', () => {
        it('calls addFavorite prop with testEmptyGem and calls preventDefault on event', () => {
            props.rowData = testEmptyGem
            props.favorite = false
            const wrapper = shallow(<Row {...props} />)

            const preventDefault = jest.fn()
            wrapper.find('button').simulate('click', { preventDefault })
            expect(props.addFavorite).toHaveBeenCalledWith(testEmptyGem)
            expect(preventDefault).toHaveBeenCalledTimes(1)
        })

        it('calls removeFavorite prop with testEmptyGem and calls preventDefault on event', () => {
            props.rowData = testEmptyGem
            props.favorite = true
            const wrapper = shallow(<Row {...props} />)

            const preventDefault = jest.fn()
            wrapper.find('button').simulate('click', { preventDefault })
            expect(props.removeFavorite).toHaveBeenCalledWith(testEmptyGem)
            expect(preventDefault).toHaveBeenCalledTimes(1)
        })
    })
})

import * as React from 'react'
import { shallow } from 'enzyme'

import List, { IProps } from 'components/core/list'
import { InfiniteLoader, AutoSizer } from 'react-virtualized'

import { testFavorites, testEmptyGem } from 'utils/fixtures'

describe('List /', () => {
    let props = {} as IProps
    const shallowAsList = () => shallow<List>(<div />)

    beforeEach(() => {
        props = {
            rowCount: 0,

            gems: [],
            favorites: testFavorites,
            addFavorite: () => ({}),
            removeFavorite: () => ({}),

            sortBy: 'downloads',
            sortDirection: 'ASC',
            sort: () => ({}),
        }
    })

    describe('rendering /', () => {
        afterEach(() => {
            if (props.gems.length) {
                expect(
                    shallow(<List {...props} />)
                        .find(InfiniteLoader)
                        .dive()
                        .find(AutoSizer)
                        .dive()
                ).toMatchSnapshot()
            } else {
                expect(shallow(<List {...props} />)).toMatchSnapshot()
            }
        })

        it('renders basic', () => undefined)

        it('renders with no gems, no isFavorites, with message', () => {
            props.gems = []
            props.isFavorites = false
        })

        it('renders with no gems, isFavorites, with message', () => {
            props.gems = []
            props.isFavorites = true
        })

        it('renders with gems, no isFavorites, with 600 height and no date column', () => {
            props.gems = [testEmptyGem]
            props.isFavorites = false
        })

        it('renders with gems, isFavorites, with 600 height and date column', () => {
            props.gems = [testEmptyGem]
            props.isFavorites = true
        })
    })

    describe('defaultProps /', () => {
        let wrapper: ReturnType<typeof shallowAsList>

        beforeEach(() => {
            wrapper = shallow(<List {...props} />)
        })

        it('should return true for isRowLoaded', () => {
            expect(wrapper.instance().props.isRowLoaded!({ index: 100000 })).toEqual(true)
        })

        it('should return null with loadMoreRows', async () => {
            expect(await wrapper.instance().props.loadMoreRows!({ startIndex: 1, stopIndex: 1 })).toEqual(
                null
            )
        })
    })

    describe('internal renderers /', () => {
        it('renders headerRenderer and is not sorted by the dataKey and DESC ordering', () => {
            const wrapper = shallow<List>(<List {...props} />)
            const fakeProps = {
                dataKey: 'info',
                sortBy: 'downloads',
                sortDirection: 'DESC' as 'DESC',
            }
            expect(wrapper.instance().headerRenderer(fakeProps)).toMatchSnapshot()
        })

        it('renders headerRenderer and is sorted by the dataKey and DESC ordering', () => {
            const wrapper = shallow<List>(<List {...props} />)
            const fakeProps = {
                dataKey: 'info',
                sortBy: 'info',
                sortDirection: 'DESC' as 'DESC',
            }
            expect(wrapper.instance().headerRenderer(fakeProps)).toMatchSnapshot()
        })

        it('renders headerRenderer and is sorted by the dataKey and ASC ordering', () => {
            const wrapper = shallow<List>(<List {...props} />)
            const fakeProps = {
                dataKey: 'info',
                sortBy: 'info',
                sortDirection: 'ASC' as 'ASC',
            }
            expect(wrapper.instance().headerRenderer(fakeProps)).toMatchSnapshot()
        })

        it('renders rowRenderer and row is not favorited', () => {
            props = { ...props, favorites: {} }
            const wrapper = shallow<List>(<List {...props} />)

            const fakeProps = {
                index: 0,
                rowData: testEmptyGem,
                className: '',
                columns: [],
                isScrolling: false,
                style: {},
            }
            expect(wrapper.instance().rowRenderer(fakeProps)).toMatchSnapshot()
        })

        it('renders rowRenderer and row is favorited', () => {
            props = { ...props, favorites: { [testEmptyGem.sha]: testEmptyGem } }
            const wrapper = shallow<List>(<List {...props} />)

            const fakeProps = {
                index: 0,
                rowData: testEmptyGem,
                className: '',
                columns: [],
                isScrolling: false,
                style: {},
            }
            expect(wrapper.instance().rowRenderer(fakeProps)).toMatchSnapshot()
        })
    })

    describe('internal handlers /', () => {
        let wrapper: ReturnType<typeof shallowAsList>

        beforeEach(() => {
            wrapper = shallow(<List {...props} gems={[testEmptyGem]} />)
        })

        it('produces correct gem with rowGetter and index 0', () => {
            expect(wrapper.instance().rowGetter({ index: 0 })).toEqual(testEmptyGem)
        })

        it('produces false with rowGetter and index out of bounds', () => {
            expect(wrapper.instance().rowGetter({ index: 150 })).toEqual(false)
        })

        it('produces correct className for header row', () => {
            expect(wrapper.instance().getRowClassName({ index: -1 })).toMatchSnapshot()
        })

        it('produces no className for non-header row', () => {
            expect(wrapper.instance().getRowClassName({ index: 150 })).toEqual('')
        })
    })
})

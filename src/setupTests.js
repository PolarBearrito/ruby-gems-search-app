import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import mockdate from 'mockdate'

import mockedFetch from 'jest-fetch-mock'

configure({ adapter: new Adapter() })

// https://stackoverflow.com/questions/32911630/how-do-i-deal-with-localstorage-in-jest-tests
class LocalStorageMock {
    constructor() {
        this.store = {}
    }

    clear() {
        this.store = {}
    }

    getItem(key) {
        return this.store[key] || null
    }

    setItem(key, value) {
        this.store[key] = value.toString()
    }

    removeItem(key) {
        delete this.store[key]
    }
}

global.localStorage = new LocalStorageMock()

mockdate.set('05/15/2020')

global.fetch = mockedFetch

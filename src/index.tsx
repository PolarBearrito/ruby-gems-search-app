import * as React from 'react'
import * as ReactDOM from 'react-dom'

import Entry from 'pages/entry'

import registerServiceWorker from 'utils/registerServiceWorker'

import 'index.css'

ReactDOM.render(<Entry />, document.getElementById('root') as HTMLElement)
registerServiceWorker()

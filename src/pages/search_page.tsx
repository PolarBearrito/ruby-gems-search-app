import React from 'react'
import SearchForm from 'components/search_form'
import SearchResults from 'components/search_results'

// types
import { History } from 'history'

interface IProps {
    history: History
}

export default class Search extends React.Component<IProps> {
    render() {
        return (
            <div>
                <SearchForm {...this.props} />
                <SearchResults {...this.props} />
            </div>
        )
    }
}

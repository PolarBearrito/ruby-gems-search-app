import * as React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import store from 'redux/create_store'

import { Switch, Route } from 'react-router-dom'

import Favorites from 'pages/favorites'
import Search from 'pages/search_page'

export default class Entry extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Provider store={store}>
                    <Switch>
                        <Route exact path="/" component={Search} />
                        <Route path="/saved" component={Favorites} />
                    </Switch>
                </Provider>
            </BrowserRouter>
        )
    }
}

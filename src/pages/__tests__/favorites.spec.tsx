import * as React from 'react'
import { shallow } from 'enzyme'

import { Favorites, IProps, mapStateToProps, mapDispatchToProps } from 'pages/favorites'

import { testGems, testFavorites, testGem, testStore } from 'utils/fixtures'

describe('Favorites /', () => {
    const props = {
        gems: testGems,
        favorites: testFavorites,
        sortBy: 'downloads',
        sortDirection: 'ASC',
        sort: () => ({}),
        addFavorite: () => ({}),
        removeFavorite: () => ({}),
    } as IProps

    describe('rendering /', () => {
        it('renders basic', () => {
            const wrapper = shallow(<Favorites {...props} />)
            expect(wrapper).toMatchSnapshot()
        })
    })

    describe('redux connect /', () => {
        it('mapsStateToProps', () => {
            jest.mock('redux/selectors')

            expect(mapStateToProps(testStore)).toMatchSnapshot()
        })

        it('mapsDispatchToProps', () => {
            const fakeDispatch = jest.fn(action => action)
            const mapped = mapDispatchToProps(fakeDispatch)

            mapped.addFavorite(testGem)

            mapped.removeFavorite(testGem)

            mapped.sort({ sortBy: 'info', sortDirection: 'DESC' })
            expect(fakeDispatch.mock.calls).toMatchSnapshot()
        })
    })
})

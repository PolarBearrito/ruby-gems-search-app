import * as React from 'react'
import { shallow } from 'enzyme'
import { createBrowserHistory } from 'history'

import Search from 'pages/search_page'

describe('Search Page /', () => {
    const props = {
        history: createBrowserHistory(),
    }

    it('renders basic', () => {
        const wrapper = shallow(<Search {...props} />)
        expect(wrapper).toMatchSnapshot()
    })
})

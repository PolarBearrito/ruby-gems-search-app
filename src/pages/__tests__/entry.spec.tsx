import * as React from 'react'
import { shallow } from 'enzyme'

import Entry from 'pages/entry'

it('Entry / renders basic', () => {
    const wrapper = shallow(<Entry />)
    expect(wrapper).toMatchSnapshot()
})

import React from 'react'
import List from 'components/core/list'
import Icon from '@mdi/react'

// helpers
import { mdiDiamondStone } from '@mdi/js'

// redux
import { connect } from 'react-redux'
import { actions as favoriteActions } from 'redux/gem_favorite/actions'
import { actions as sortByActions } from 'redux/sort_by/actions'
import { selectSortedFavorites } from 'redux/selectors'

// types
import { Dispatch, AnyAction } from 'redux'
import { SortDirectionType } from 'react-virtualized'
import { IGemDict } from 'redux/gem_favorite/reducer'
import { IGem, SORT_BY_KEYS, ISortBySetting } from 'redux/types'
import { ICombinedStoreState } from 'redux/create_store'

import styles from 'components/core/app.module.scss'

interface IReduxState {
    gems: IGem[]
    favorites: IGemDict
    sortBy: SORT_BY_KEYS
    sortDirection: SortDirectionType
}

interface IReduxDispatch {
    addFavorite: (gem: IGem) => void
    removeFavorite: (gem: IGem) => void
    sort: (sortBySetting: ISortBySetting) => void
}

export interface IProps extends IReduxState, IReduxDispatch {}

export class Favorites extends React.Component<IProps> {
    render() {
        const { gems } = this.props
        return (
            <div>
                <div className={styles.header}>
                    <div className={styles.title}>
                        <Icon path={mdiDiamondStone} size={2} color="rgba(255, 100, 100, 1)" />
                        Saved Gems
                    </div>
                </div>
                <List {...this.props} rowCount={gems.length} isFavorites />
            </div>
        )
    }
}

export const mapStateToProps = (state: ICombinedStoreState) => ({
    gems: selectSortedFavorites(state),
    favorites: state.favorites.gems,
    ...state.sort_by.favorites,
})

export const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => {
    return {
        addFavorite: (gem: IGem) => dispatch(favoriteActions.addFavorite(gem)),
        removeFavorite: (gem: IGem) => dispatch(favoriteActions.removeFavorite(gem)),
        sort: (sortBySetting: ISortBySetting) =>
            dispatch(sortByActions.setSortBy('favorites', sortBySetting)),
    }
}

export default connect<IReduxState, IReduxDispatch>(
    mapStateToProps,
    mapDispatchToProps
)(Favorites)

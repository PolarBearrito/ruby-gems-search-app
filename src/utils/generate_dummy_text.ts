// take the index and map it to a random value using it's binary string representation
// this is so we can generate random length dummy grey loading text simply based on the index
export default (index: number, minLength: number, increment: number = 15) =>
    `${((index * index * index)
        .toString(2)
        .split('')
        .filter(l => (index % 2 === 0 ? l === '1' : l === '0')).length %
        10) *
        increment +
        minLength}px`

import { initialState as searchStoreState } from 'redux/gem_search/reducer'
import { initialState as favoritesStoreState } from 'redux/gem_favorite/reducer'
import { initialState as sortByStoreState } from 'redux/sort_by/reducer'

export const testFavorites = {
    '7176401ea00d0579305b06cfdffd076872a96c8241161a01a4baffa3104c0be4': {
        name: 'rubyconfbd2013_hello',
        downloads: 2022,
        version: '0.0.1',
        info: 'A very basic gem demonstration for RubyConfBD 2013',
        sha: '7176401ea00d0579305b06cfdffd076872a96c8241161a01a4baffa3104c0be4',
        project_uri: 'https://rubygems.org/gems/rubyconfbd2013_hello',
        date_saved: '2018-12-01T04:46:34.018Z',
    },
    '56dcad94920a090bc6f351c0a2b114369a64018df5830886447c1d5da058aed3': {
        name: 'sfba_transit',
        downloads: 1946,
        version: '0.0.0',
        info: 'Fetch information about transit card ("clipper") status, balance, and transactions.',
        sha: '56dcad94920a090bc6f351c0a2b114369a64018df5830886447c1d5da058aed3',
        project_uri: 'https://rubygems.org/gems/sfba_transit',
        date_saved: '2018-12-01T04:46:34.851Z',
    },
    f2b73d7a3fce13934ea7301e6be8a317b5476ea4cbd4f5ac68fc25946e09cdfd: {
        name: 'fbox',
        downloads: 2621,
        version: '0.1.2',
        info:
            'Fbox - FaucetBox REST API helper. It allows you to integrate FaucetBox API in an easy way in your RubyOnRails application.',

        sha: 'f2b73d7a3fce13934ea7301e6be8a317b5476ea4cbd4f5ac68fc25946e09cdfd',
        project_uri: 'https://rubygems.org/gems/fbox',
        date_saved: '2018-12-01T04:46:36.894Z',
    },
}

export const testGems = Object.values(testFavorites)

export const testGem = {
    name: 'testGem',
    downloads: 2046,
    version: '0.1.0',
    info:
        'The Fpb gem provides support for Flow Based Programming for Ruby.\nFlow Based programming is described at http://www.jpaulmorrison.com/fbp\n',
    sha: '080183a074f6bfe1773fbf72f8fd88972ca33a7f0946935dd7fff3def81eb403',
    project_uri: 'https://rubygems.org/gems/fbp',
}

export const testEmptyGem = {
    name: '',
    downloads: 0,
    version: '',
    info: '',
    sha: '',
    project_uri: '',
}

export const testGem2 = {
    name: 'testGem2',
    downloads: 9001,
    version: '0.1.0',
    info: 'simple',
    sha: 'abc123',
    project_uri: 'https://rubygems.org/gems/test-gem',
}

export const testUnsortedFavorites = {
    5: {
        name: '1',
        downloads: 2,
        version: '3',
        info: '4',
        sha: '5',
        project_uri: '6',
        date_saved: '2018-12-01T04:46:36.895Z',
    },
    2: {
        name: '6',
        downloads: 5,
        version: '4',
        info: '3',
        sha: '2',
        project_uri: '1',
        date_saved: '2018-12-01T04:46:36.894Z',
    },
    cde: {
        name: '28',
        downloads: 49,
        version: '2.0',
        info: 'abc',
        sha: 'cde',
        project_uri: 'ab',
        date_saved: '2016-01-03T20:46:36.894Z',
    },
    smn: {
        name: 'kf',
        downloads: 9001,
        version: '3.0',
        info: 'zzz',
        sha: 'smn',
        project_uri: 'gj',
        date_saved: '2016-01-03T16:46:36.894Z',
    },
}

export const testStore = {
    search: searchStoreState,
    favorites: favoritesStoreState,
    sort_by: sortByStoreState,
}
